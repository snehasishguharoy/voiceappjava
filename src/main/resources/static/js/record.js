

function restore(){
  $("#record, #live").removeClass("disabled");
  $("#pause").replaceWith('<a class="button one" id="pause">Pause</a>');
  $(".one").addClass("disabled");
  Fr.voice.stop();
}

function recordWav(){
    elem = $(this);
    Fr.voice.record($("#live").is(":checked"), function(){
      elem.addClass("disabled");
      $("#live").addClass("disabled");
      $(".one").removeClass("disabled");
    });
  }

function pause(){
    if($(this).hasClass("resume")){
      Fr.voice.resume();
      $(this).replaceWith('<a class="button one" id="pause">Pause</a>');
    }else{
      Fr.voice.pause();
      $(this).replaceWith('<a class="button one resume" id="pause">Resume</a>');
    }
  }

function incrementTime()
{
switchMicrophoneOff();

  Fr.voice.pause();
  if ($("#username").val()=="")
	  uploadWav();
  else
	  uploadNewUserWav();
}

function switchMicrophoneOn()
{
	$("#recordImg").removeClass("recordoff");
	$("#recordImg").addClass("recording");
}

function switchMicrophoneOff()
{
	$("#recordImg").removeClass("recording");
	$("#recordImg").addClass("recordoff");
}

function getvoice()
{
	$("#username").val("");
	recordWav();
}

function getvoicenewuser()
{
   recordWav();
}

function uploadWav(){
    Fr.voice.export(function(blob){
      var formData = new FormData();
      formData.append('file', blob);
      formData.append('username', $("#userList").val());
  
      $.ajax({
        url: "http://localhost:8080/upload",
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function(output) {        	
        	console.log(output.processed);
        	$("#output").html(output.processed)
        	if (output.processed=='OK')
        		$("#validationMatch").attr("src", "img/smileok.png");
        	else
        		$("#validationMatch").attr("src", "img/smileko.png");
        }
      });
    }, "blob");
    restore();
  }

function uploadNewUserWav(){
	Fr.voice.export(function(blob){
	      var formData = new FormData();
	      formData.append('file', blob);
	      formData.append('username', $("#username").val());
	      $.ajax({
	        url: "http://localhost:8080/uploadnewuser",
	        type: 'POST',
	        data: formData,
	        contentType: false,
	        processData: false,
	        success: function(output) {        	
	        	console.log(output.processed);
	        	alert(output.processed);
	        // getUserList();
	        }
	      });
	    }, "blob");
	    restore();
	  }

function getUserList(){
	$.ajax({
        url: "http://localhost:8080/getuserlist",
        type: 'GET',
        contentType: false,
        processData: false,
        success: function(output) {        	
        	console.log(output.processed);
        	$("#userList").html(output.processed)
        }
      });
}
	
$(document).ready(function(){
	$(document).on("click", "#getvoice", getvoice);
	$(document).on("click", "#getvoicenewuser", getvoicenewuser);
	// getUserList();

});
