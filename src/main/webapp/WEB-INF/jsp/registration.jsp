<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form by Colorlib</title>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Main css -->
<link rel="stylesheet" href="css/style.css">
<style>
body {
	font-size: 10px;
	text-decoration: none !important;
	font-family: Verdana;
}

.button {
	margin: 0px 5px;
	padding: 5px 12px;
	cursor: pointer;
	outline: none;
	font-size: 13px;
	text-decoration: none !important;
	text-align: center;
	color: #fff;
	background-color: #4D90FE;
	background-image: linear-gradient(top, #4D90FE, #4787ED);
	background-image: -ms-linear-gradient(top, #4D90FE, #4787ED);
	background-image: -o-linear-gradient(top, #4D90FE, #4787ED);
	background-image: linear-gradient(top, #4D90FE, #4787ED);
	border: 1px solid #4787ED;
	box-shadow: 0 1px 3px #BFBFBF;
}

a.button {
	color: #fff;
}

.recording {
	background: rgba(0, 0, 0, 0)
		radial-gradient(ellipse farthest-corner at center center, #ff0000 0%,
		#ffffff 75%, #ffffff 100%, #7db9e8 100%) repeat scroll 0 0;
}

.recordoff {
	background: #ffffff repeat scroll 0 0;
}
</style>
<!-- JS -->
<script src="js/recorder.js"></script>
<script src="js/Fr.voice.js"></script>
<script src="js/jquery.js"></script>
<script src="js/record.js"></script>
<script type="js/main.js"></script>
<script>
	$(document).ready(function() {
		$('#reset').click(function() {
			if (confirm("Want to the username?")) {
				$('input[name="username"]').val('');
			}
		});
		$('input').on(focus, function() {
			var input = $(this);
			var is_name = input.val();
			console.log(is_name);
		});

	});
</script>
</head>
<body>
	<div class="main">
		<div class="container">
			<div class="signup-content">
				<div class="signup-img">
					<img src="img/form-img.jpg" alt="">
					<div class="signup-img-content">
						<h2>Register Your Voice</h2>
						<h2>while user accounts are available !</h2>
					</div>
				</div>
				<div class="signup-form">
					<div class="form-row">
						<div class="form-group">
							<div class="form-input">
								<div class="username">
									<label for="first_name" class="required" style="margin: 10px;">User
										Name</label> <input style="margin: 10px;" type="text" name="username"
										id="username" />
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group">
							<div class="form-input">
								<img id="recordImg" src="img/mic128.png">
								<div style="border: 1px solid gray; margin: 20px 20px;">
									<div style="margin: 10px;">
										<div class="record">
											<b>Record!</b><br> Record your voice for authentication.
											<br> &nbsp;&nbsp;&nbsp;&nbsp;- Set a user key ("Marco",
											"Gianni","Ferninando", etc...) for the UserName and click
											"Upload User" button<br> &nbsp;&nbsp;&nbsp;&nbsp;- Allow
											the browser to share your microphone with the Web Application<br>
											&nbsp;&nbsp;&nbsp;&nbsp;- The microphone image with red
											background indicates that your voice is recording. After 4
											seconds the record is stopped.<br> <br> <b>Congratulations,
												you've recorded your voice</b><br>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group">
							<div class="form-input">
								<div class="form-submit" style="margin: 20px 20px;">
									<input type="submit" value="Register Here" class="submit"
										id="getvoicenewuser" name="getvoicenewuser" /> <input
										type="submit" value="Reset" class="submit" id="reset"
										name="reset" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

