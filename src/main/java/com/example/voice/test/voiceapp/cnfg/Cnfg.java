package com.example.voice.test.voiceapp.cnfg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Configuration
public class Cnfg {
	
	@Bean(name = "viewJson")
	public MappingJackson2JsonView jsonView() {
	  final MappingJackson2JsonView mappingJacksonJsonView = new MappingJackson2JsonView();
	  mappingJacksonJsonView.setContentType("application/json");
	  return mappingJacksonJsonView;
	}

}
