package com.example.voice.test.voiceapp.cntlr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.bitsinharmony.recognito.MatchResult;
import com.example.voice.test.voiceapp.dto.Voice;

@Controller
public class RecognitoController {

	@Autowired
	private Voice voice;

	private static final Log logger = LogFactory.getLog(RecognitoController.class);

	@GetMapping("/reg")
	public String getIndex() {
		return "registration";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/uploadnewuser")
	public ModelAndView uploadNewUserWave(@RequestParam("file") MultipartFile file,
			@RequestParam("username") String username, HttpServletRequest request, Model model)
			throws IOException, UnsupportedAudioFileException {
		if (null == file && username == null) {
			return null;
		}

		String result = null;
		try {
			byte[] valueDecoded = file.getBytes();

			String filePath = "C:\\Users\\sony\\Desktop\\Voice\\audio\\" + username + ".wav";
			// "/audio/" + username + ".wav");

			logger.info("Upload User:" + username + " File:" + filePath);

			File sample = new File(filePath);

			FileOutputStream os = new FileOutputStream(sample);
			os.write(valueDecoded);
			Voice voiceMatch = new Voice("C:\\Users\\sony\\Desktop\\Voice\\audio\\");
			os.close();
			// model.addAttribute("processed", username + " - OK");
			result = username + " - OK";
			return new ModelAndView("viewJson", "processed", "The user "+username + " has successfully registered user account with  voice");
		} catch (Exception e) {
			logger.error("Errore", e);
			// model.addAttribute("processed", "Error!" + e.getMessage());
			result = "Error!" + e.getMessage();
			return new ModelAndView("viewJson", "processed", "Error!" + e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/upload")
	public ModelAndView uploadWave(@RequestParam("file") MultipartFile file, @RequestParam("username") String username,
			HttpServletRequest request, Model model) throws IOException, UnsupportedAudioFileException {

		byte[] valueDecoded = file.getBytes();

		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String today = dateFormat.format(date); // 2013/10/15 16:16:39
		today = today.replace(":", "_");

		String filePath = "C:\\Users\\sony\\Desktop\\Voice\\audio" + today + ".wav";

		File sample = new File(filePath);

		FileOutputStream os = new FileOutputStream(sample);
		os.write(valueDecoded);
		os.close();

		String filePathMatch = "C:\\Users\\sony\\Desktop\\Voice\\audio\\" + username;

		Voice voiceMatch = new Voice(username, filePathMatch);

		List<MatchResult<String>> matches = voiceMatch.recognito.identify(sample);

		logger.info("");
		logger.info("****************");
		logger.info("Verify User:" + username + " File:" + filePath);
		logger.info("");

		logger.info("Sample " + filePath);

		StringBuilder sb = new StringBuilder();

		MatchResult<String> result = matches.get(0);

		if (result.getDistance() < 0.1)
			sb.append("OK");
		else
			sb.append("KO");

		logger.info("Identified: " + result.getKey() + " distance of " + result.getDistance() + " with "
				+ result.getLikelihoodRatio() + "% positive about it...");
		model.addAttribute("processed", sb.toString());
		// return sb.toString();
		return new ModelAndView("viewJson", "processed", sb.toString());

	}

	@RequestMapping(method = RequestMethod.GET, value = "/getuserlist")
	public ModelAndView getuserlist(HttpServletRequest request, Model model) throws IOException {
		// model.addAttribute("processed", voice.getOptionUser());
		// return voice.getOptionUser().replace(".wav", "");
		return new ModelAndView("viewJson", "processed", voice.getOptionUser());
	}

}
